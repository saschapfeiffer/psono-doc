# PSONO Documentation

# Canonical source

The canonical source of PSONO Server is [hosted on GitLab.com](https://gitlab.com/psono/psono-doc).

# Preamble
Test
Psono docs are driven by Vuepress

This whole guide is based on Ubuntu 18.04 LTS. Ubuntu 12.04+ LTS and Debian based systems should be similar if not even
identical.

# General

The current build can be found here:

https://doc.psono.com/

# Installation

To install all dependencies run the following command:

	yarn install
	npm i -g raml2html raml2html-slate-theme

# Start the development server

To start the development server execute the following command:

	yarn docs:dev

# Create build

To build the production build execute the following command:

	yarn docs:build

# LICENSE

Visit the [License.md](/LICENSE.md) for more details