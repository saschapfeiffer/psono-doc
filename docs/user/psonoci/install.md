---
title: Install Psonoci
metaTitle: Install Psonoci | Psono Documentation
meta:
  - name: description
    content: Install instruction for Psonoci, the psono CI / CD integration utility
---

# Install Psonoci

Install instruction for Psonoci, the psono CI / CD integration utility

## Preamble

At this point we assume that you already have a psono server running, ready to accept connections, have it filled with 
secrets and want to integrate Psono now into your build pipeline to secure your secrets with a second layer of defense.
We further assume that you have a restricted API key configured. If not follow the [guide to create an API key](/user/api-key/creation.html).

## Installation

TBD