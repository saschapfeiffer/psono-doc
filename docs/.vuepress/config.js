module.exports = {
  title: 'Psono Documentation',
  description: 'These brief instructions will help you get started quickly with the Psono password manager',
  locales: {
    '/': {
      lang: 'en-US',
      title: 'Psono Documentation',
      description: 'These brief instructions will help you get started quickly with the Psono password manager.'
    }
  },
  plugins: {
    'sitemap': {
      hostname: 'https://doc.psono.com'
    },
    '@vuepress/google-analytics': {
      'ga': 'UA-85002864-2'
    }
  },
  themeConfig: {
    logo: '/images/logo.png',
    smoothScroll: true,
    repo: 'https://gitlab.com/psono',
    // if your docs are in a different repo from your main project:
    docsRepo: 'https://gitlab.com/psono/psono-doc',
    // if your docs are not at the root of the repo:
    docsDir: 'docs',
    // if your docs are in a specific branch (defaults to 'master'):
    docsBranch: 'develop',
    // defaults to false, set to true to enable
    editLinks: true,
    // custom text for edit link. Defaults to "Edit this page"
    editLinkText: 'Edit this page',
    locales: {
      '/': {
        selectText: 'Languages',
        label: 'English',
        serviceWorker: {
          updatePopup: {
            message: "New content is available.",
            buttonText: "Refresh"
          }
        },
        algolia: {},
        nav: [
          { text: 'Home', link: '/' },
          {
            text: 'More',
            ariaLabel: 'More',
            items: [
              {
                text: 'Users:',
                items: [
                  {
                    text: 'Getting Started',
                    link: '/user/getting-started/overview'
                  },
                  {
                    text: 'Api Key',
                    link: '/user/api-key/overview'
                  },
                  {
                    text: 'File Repository',
                    link: '/user/file-repository/overview'
                  },
                  {
                    text: 'Two Factor Authentication',
                    link: '/user/two-factor-authentication/duo'
                  },
                  {
                    text: 'Other',
                    link: '/user/other/emergency-codes'
                  }
                ]
              },
              {
                text: 'Admins:',
                items: [
                  {
                    text: 'Summary',
                    link: '/admin/overview/summary'
                  },
                  {
                    text: 'Installation',
                    link: '/admin/installation/install-preparation'
                  },
                  {
                    text: 'Configuration',
                    link: '/admin/configuration/audit-log'
                  },
                  {
                    text: 'Update',
                    link: '/admin/update/update-server-ce'
                  },
                  {
                    text: 'FAQ',
                    link: '/admin/faq/faq'
                  },
                  {
                    text: 'ASVS',
                    link: '/admin/asvs/overview'
                  },
                  {
                    text: 'Other',
                    link: '/admin/other/commands'
                  }
                ]
              },
              {
                text: 'Developer:',
                items: [
                  {
                    text: 'General',
                    link: '/admin/development/contribution-agreement'
                  },
                  { text: 'API', link: 'https://doc.psono.com/api.html' }
                ]
              }
            ]
          },
          { text: 'Legal Notice', link: '/legal-notice' },
          { text: 'Privacy Policy', link: '/privacy-policy' },
          { text: 'Psono.com', link: 'https://psono.com/' }
        ],
        sidebar: {
          '/admin/': [
            {
              title: 'Overview',
              collapsable: false,
              children: [
                'overview/summary',
                'overview/introduction',
                'overview/supported-features',
                'overview/about',
                'overview/support'
              ]
            },
            {
              title: 'Installation',
              collapsable: false,
              children: [
                'installation/install-preparation',
                'installation/install-postgres-db',
                'installation/install-server-ce',
                'installation/install-server-ee',
                'installation/install-webclient',
                'installation/install-admin-webclient',
                'installation/install-reverse-proxy',
                'installation/install-browser-extension',
                'installation/install-mobile-app',
                'installation/install-fileserver'
              ]
            },
            {
              title: 'Configuration',
              collapsable: false,
              children: [
                'configuration/audit-log',
                'configuration/compliance-settings',
                'configuration/email-amazon-ses',
                'configuration/email-mailgun',
                'configuration/email-mailjet',
                'configuration/email-mandrill',
                'configuration/email-postmark',
                'configuration/email-sendgrid',
                'configuration/email-sendinblue',
                'configuration/email-smtp',
                'configuration/email-sparkpost',
                'configuration/ldap-group-mapping',
                'configuration/ldap-ad',
                'configuration/ldap-freeipa',
                'configuration/ldap-openldap',
                'configuration/ldaps-custom-ca',
                'configuration/saml-group-mapping',
                'configuration/saml-adfs',
                'configuration/saml-aws',
                'configuration/saml-azure-ad',
                'configuration/saml-okta'
              ]
            },
            {
              title: 'Update',
              collapsable: false,
              children: [
                'update/update-server-ce',
                'update/update-server-ee',
                'update/update-webclient',
                'update/update-admin-client',
                'update/update-fileserver',
              ]
            },
            {
              title: 'Development',
              collapsable: false,
              children: [
                'development/contribution-agreement',
                'development/server',
                'development/webclient',
                'development/browser-extension',
                'development/entity-model',
                'development/datastore-structure',
                'development/cryptography',
                'development/build-pipeline'
              ]
            },
            {
              title: 'ASVS',
              collapsable: false,
              children: [
                'asvs/overview',
                'asvs/v01-architecture',
                'asvs/v02-authentication',
                'asvs/v03-session-management',
                'asvs/v04-access-control',
                'asvs/v05-malicious-input',
                'asvs/v06-output-encoding',
                'asvs/v07-cryptography',
                'asvs/v08-error-handling',
                'asvs/v09-data-protection',
                'asvs/v10-communications',
                'asvs/v11-http-security',
                'asvs/v12-security-configuration',
                'asvs/v13-malicious-controls',
                'asvs/v14-internal-security',
                'asvs/v15-business-logic-flaws',
                'asvs/v16-files-and-resources',
                'asvs/v17-mobile',
                'asvs/v18-web-services',
                'asvs/v19-configuration',
                'asvs/stride'
              ]
            },
            {
              title: 'FAQ',
              collapsable: false,
              children: [
                'faq/faq'
              ]
            },
            {
              title: 'Other',
              collapsable: false,
              children: [
                'other/commands',
                'other/healthcheck'
              ]
            }
          ],
          '/user/': [
            {
              title: 'Getting started',
              collapsable: false,
              children: [
                'getting-started/overview',
                'getting-started/features'
              ]
            },
            {
              title: 'API Key',
              collapsable: false,
              children: [
                'api-key/overview',
                'api-key/creation',
                'api-key/usage-with-session',
                'api-key/usage-without-session-and-local-decryption',
                'api-key/usage-without-session-and-remote-decryption'
              ]
            },
            {
              title: 'File Repository',
              collapsable: false,
              children: [
                'file-repository/overview',
                'file-repository/setup-aws-as-file-repository',
                'file-repository/setup-do-as-file-repository',
                'file-repository/setup-gcs-as-file-repository',
                'file-repository/share-file-repositories'
              ]
            },
            {
              title: 'Two Factor Authentication',
              collapsable: false,
              children: [
                'two-factor-authentication/duo',
                'two-factor-authentication/google-authenticator',
                'two-factor-authentication/yubikey'
              ]
            },
            {
              title: 'Psonoci for CI / CD',
              collapsable: false,
              children: [
                'psonoci/install',
                'psonoci/usage'
              ]
            },
            {
              title: 'Other',
              collapsable: false,
              children: [
                'other/emergency-codes',
                'other/export',
                'other/import',
                'other/recovery-codes'
              ]
            }
          ],
          '/': [
            {
              title: 'Home',
              collapsable: false,
              children: [
                '',
              ]
            }
          ]
        }
      }
    }
  },
  dest: './build',
};