---
title: 6. Install Browser Extension (optional)
metaTitle: Install Psono Browser Extension | Psono Documentation
meta:
  - name: description
    content: Installation guide for the Psono browser extensions for Chrome or Firefox.
---

# Install Browser Extension

Installation guide for the Psono browser extensions for Chrome or Firefox.

To install one of our browser extensions, follow the appropriate guide below.

## Install Chrome extension

- Open your Chrome browser
- Visit the following url:

[chrome.google.com/webstore/detail/psonopw/eljmjmgjkbmpmfljlmklcfineebidmlo](https://chrome.google.com/webstore/detail/psonopw/eljmjmgjkbmpmfljlmklcfineebidmlo)


## Install Firefox extension

- Open your Firefox browser
- Visit the following url:

[addons.mozilla.org/de/firefox/addon/psono-pw-password-manager/](https://addons.mozilla.org/de/firefox/addon/psono-pw-password-manager/)


## Windows

If you are in an Windows enterprise environment, you can roll out psono in your complete environment and preconfigure extensions with group policies.


### Chrome

#### Automated Deployment

For instructions how to roll out extensions with Group Policies and Chrome you can find some decent instructions here:

[https://support.google.com/chrome/a/answer/7532015?hl=en](https://support.google.com/chrome/a/answer/7532015?hl=en)

The extension ID that you need is `eljmjmgjkbmpmfljlmklcfineebidmlo`

#### Pre-configuration:

In order to preconfigure the extension to point to your server, you have to create a key in your users registry called "ConfigJson" in `HKLM\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy` as shown on this image:

![Registry](/images/admin/install_browser_extension/registry_chrome.jpg)

The value of this key, is your `config.json` that you created previously for the webclient (with some escaped chars).

To automate this, you can put the following content into a psono.reg and execute it:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy]
"ConfigJson"="{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
```

### Firefox

#### Automated Deployment

[https://support.mozilla.org/en-US/kb/customizing-firefox-using-group-policy-windows](https://support.mozilla.org/en-US/kb/customizing-firefox-using-group-policy-windows)

The extension ID that you need is `{3dce78ca-2a07-4017-9111-998d4f826625}`. (The brackets are part of the ID)

#### Pre-configuration:

In order to preconfigure the extension to point to your server, you have to create the following key `HKEY_LOCAL_MACHINE\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}` with a Default Value being a path pointing to a json file as shown on this image:

![Registry](/images/admin/install_browser_extension/registry_firefox.jpg)

To automate this, you can put the following content into a psono.reg and execute it:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}]
@="C:\\Program Files (x86)\\Mozilla Firefox\\browser\\extensions\\3dce78ca-2a07-4017-9111-998d4f826625.json"
```

Under `C:\Program Files (x86)\Mozilla Firefox\browser\extensions` you create a `3dce78ca-2a07-4017-9111-998d4f826625.json` file with this content:

```json
{
    "name": "{3dce78ca-2a07-4017-9111-998d4f826625}",
    "description": "ignored",
    "type": "storage",
    "data": {
        "ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
    }
}
```

The value of the "ConfigJson" property is your `config.json` that you created previously for the webclient (with some escaped chars).


### Edge (based on Chromium)

#### Automated Deployment

For instructions how to roll out extensions with Group Policies and Edge you can find some decent instructions here:

[https://docs.microsoft.com/en-us/DeployEdge/deploy-edge-plan-deployment](https://docs.microsoft.com/en-us/DeployEdge/deploy-edge-plan-deployment)

The extension ID that you need is `eljmjmgjkbmpmfljlmklcfineebidmlo`

#### Pre-configuration:

In order to preconfigure the extension to point to your server, you have to create a key in your users registry called "ConfigJson" in `HKLM\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy` as shown on this image:

![Registry](/images/admin/install_browser_extension/registry_edge.jpg)

The value of this key, is your `config.json` that you created previously for the webclient (with some escaped chars).

To automate this, you can put the following content into a psono.reg and execute it:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy]
"ConfigJson"="{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
```

## Linux

If you are in an enterprise environment with a lot of linux clients, you can preconfigure the extension with your server with the following instructions.

### Chrome

#### Pre-configuration:

For chrome create a `psono.json` in `/etc/opt/chrome/policies/managed` with the following content:

```json
{
    "3rdparty": {
        "extensions": {
            "eljmjmgjkbmpmfljlmklcfineebidmlo": {
                "ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
            }
        }
    }
}
```

The value of the "ConfigJson" property is your `config.json` that you created previously for the webclient (with some escaped chars).

### Firefox

#### Pre-configuration:

For chrome create a `{3dce78ca-2a07-4017-9111-998d4f826625}.json` in `/usr/lib/mozilla/managed-storage` with the following content:

```json
{
    "name": "{3dce78ca-2a07-4017-9111-998d4f826625}",
    "description": "ignored",
    "type": "storage",
    "data": {
        "ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
    }
}
```

The value of the "ConfigJson" property is your `config.json` that you created previously for the webclient (with some escaped chars).

