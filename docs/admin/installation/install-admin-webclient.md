---
title: 4. Install Admin Webclient (optional)
metaTitle: Install Psono Admin Webclient | Psono Documentation
meta:
  - name: description
    content: Overview of how to use the API Key.
---

# Install Admin Webclient

Installation guide of the Psono Admin Webclient

## Preamble

The admin webclient is a stateless website, allowing administrators to administrate Psono without the need to install anything
(besides a browser). It can be served by any web server, we prefer Nginx, but any other webserver (Apache, IIS) is fine too.
The admin web client is optional. As an alternative administrators can use the command line.

## Installation with Docker

The latest build of our Admin Web Client as a docker image can be found here: [hub.docker.com/r/psono/psono-admin-client/](https://hub.docker.com/r/psono/psono-admin-client/)
Follow belows instructions to bring it online.

1.  Run the docker image and expose the port

    ```bash
    docker run --name psono-admin-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10102:80 psono/psono-admin-client:latest
    ```

	The `config.json` is the same one as you were using before with the normal client.

	This will now start the psono admin client on port `10102`.

    If you open now `http://your-ip:10102` you should see a beautiful login screen.
    If not, please make sure you have no firewall on the server blocking you.

2.  Setup nginx (or apache) relay

	A good webserver config is essential for your security. If you have chosen nginx as your webserver, then a suitable config can be found here.
	You have the choice to run the portal directly at the root of your domain (or subdomain):

    ```
    server {
        ...
        location ~* \.(?:ico|css|js|gif|jpe?g|png|eot|woff|woff2|ttf|svg|otf)$ {
            expires                 30d;
            add_header              Pragma public;
            add_header              Cache-Control "public";
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;

            proxy_pass              http://localhost:10102;
            proxy_redirect          http://localhost:10102 https://example.com;
        }

        location / {
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;

            proxy_read_timeout      90;

            proxy_pass              http://localhost:10102;
            proxy_redirect          http://localhost:10101 https://example.com;
        }
    }
    ```

	Or if you want to install the server on the same domain (or subdomain) as your user client, then you can use the following config:

    ```
    server {
        ...
        location ~* ^/portal.*\.(?:ico|css|js|gif|jpe?g|png|eot|woff|woff2|ttf|svg|otf)$ {
            expires 30d;
            add_header              Pragma public;
            add_header              Cache-Control "public";
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;

            proxy_pass              http://localhost:10102;
            proxy_redirect          http://localhost:10102 https://example.com;
        }

        location /portal {
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;

            proxy_read_timeout      90;

            proxy_pass              http://localhost:10102;
            proxy_redirect          http://localhost:10102 https://example.com;
        }
    }
    ```

    This config assumes that the webclient is running on localhost port `10102`.

3.  Promote your admin user

	As a next step you have to promote your user to become a superuser (at least for the first one).
	To do that, run the following command on one of your server instances:

	If you installed the server with docker:

    ```bash
    docker run --rm \
      -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py promoteuser username@example.com superuser
    ```

	If you installed the server without docker:

    ```bash
    python3 ~/psono-server/psono/manage.py promoteuser username@example.com superuser
    ```




4.  Enable Management API

	Verify that you have the following line on your server in your settings.yaml to enable the management API

    ```yaml
	MANAGEMENT_ENABLED: True
	```

    Don't forget to restart your server, after you added it.

5.  Setup Reverse Proxy

	To run the admin portal in production, a reverse proxy is needed, to handle the ssl offloading.
	Follow the [guide to setup reverse proxy](/admin/installation/install-reverse-proxy.html) as a next step.

## Installation without Docker

The webclient is a pure html / js website, that can be hosted with any webserver and has zero dependencies.

1.  Download the admin artifact

	Visit the following url and download the admin webclient:

	[psono.jfrog.io/psono/psono/admin-client/latest/webclient.zip](https://psono.jfrog.io/psono/psono/admin-client/latest/webclient.zip)

2.  Install webclient

    Create a folder called `portal` in your htdocs directory of your webserver and unpack the webclient into that folder.

    ::: tip
    Other paths will not work.
    :::

3.  Update config.json

    In the `portal` folder you will find a config.json. You can copy the config.json from your normal web client and replace the one in the `portal` folder

    ::: tip
    Other paths will not work.
    :::

4.  Promote your admin user

	As a next step you have to promote your user to become a superuser (at least for the first one).
	To do that, run the following command on one of your server instances:

	If you installed the server with docker:

    ```bash
    docker run --rm \
      -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py promoteuser username@example.com superuser
    ```

	If you installed the server without docker:

    ```bash
    python3 ~/psono-server/psono/manage.py promoteuser username@example.com superuser
    ```


5.  Enable Management API

	Verify that you have the following line on your server in your settings.yaml to enable the management API

    ```yaml
	MANAGEMENT_ENABLED: True
	```

    Don't forget to restart your server, after you added it.

6.  Setup Reverse Proxy

	To run the admin portal in production, you now have to configure your webserver with SSL.
	Follow the [guide to setup reverse proxy](/admin/installation/install-reverse-proxy.html) as a next step.
