---
title: About
metaTitle: About | Psono Documentation
meta:
  - name: description
    content: About the author and the psono password manager
---

# About

My name is Sascha Pfeiffer. I'm a developer and system administrator based in Erlangen, Germany.

I have started developing Psono, because I was tired of the existing solutions with their small feature sets and limitations:

* not open source
* bugs and flaws
* no sharing features
* required installation of software
* questionable "security"
* no self-hosting capability

I have developed this password manager over the course of many years and in the end shared it with the open source community.

The main "product" website is [psono.com](https://psono.com/)

A hosted version of the password manager can be found on [psono.pw](https://www.psono.pw/)

If you want to get in contact with us, write to support@psono.com
